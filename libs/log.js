import fs from 'fs';

export const log = (text) => {
  console.log(text)
  const stream = fs.createWriteStream(process.env.LOG_PATH, { flags: 'a' })
  stream.write(text + '\r\n');
}
