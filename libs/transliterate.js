import CyrillicToTranslit from 'cyrillic-to-translit-js'

const cyrillicToTranslit = new CyrillicToTranslit()

export const transliterate = (text) =>
  cyrillicToTranslit.transform(text, '_').replace('-', '_').toLowerCase()
