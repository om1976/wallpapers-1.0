import mongodb from 'mongodb'
import dotenv from 'dotenv'

dotenv.config()

export const client = await mongodb.MongoClient.connect(
  process.env.MONGO_CONNECT_URL, { useNewUrlParser: true, useUnifiedTopology: true }
)
export const db = client.db(process.env.MONGO_DB)
