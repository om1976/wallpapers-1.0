import axios from 'axios'
import dotenv from 'dotenv'
import { client, db } from './libs/mongo.js'
import { log } from './libs/log.js'

dotenv.config()

const processRemoteFile = async (url) => axios.post(
  `${process.env.HOST_IMAGES_UPLOAD}${process.env.PATH_IMAGES_REMOTE_UPLOAD}`,
  { url }
)
  .then(({ data }) => data)
  .catch((response) => ({ error: response.statusText }))

const imagesCollection = db.collection(process.env.MONGO_MOB_IMAGES_COLLECTION)
const items = await imagesCollection
  .find({ url: { $exists: false } })
  .toArray()

const { length } = items || []

for (let i = 0; i < length; i++) {
  const uploadUrl = `${process.env.HOST_MOB_IMAGES}/pic/v2/gallery/real/${items[i]['src']}-${items[i]['_id']}.jpg`
  const result = await processRemoteFile(uploadUrl)
  if (result.error) {
    log(`${items[i]['_id']} -- upload error`)
    continue
  }
  const { url, metadata } = result
  const { size, width, height } = metadata || {}
  await imagesCollection.updateOne({ _id: items[i]['_id'] }, { $set: { url, size, width, height }})
    .catch(() => log(`${items[i]['_id']} -- update error`))

  console.log(items[i]['_id'])
}

client.close()
