import dotenv from 'dotenv'
import { client, db } from './libs/mongo.js'
import { log } from './libs/log.js';

dotenv.config()

const supportedLocales = ['en', 'ru', 'es', 'pt', 'fr', 'de', 'ua', 'ja', 'cn']

const imagesCollection = db.collection(process.env.MONGO_MOB_IMAGES_COLLECTION)
const imagesTagsCollection = db.collection(process.env.MONGO_MOB_IMAGES_TAGS_COLLECTION)
const items = await imagesCollection.find({ _id: { $gt: 50379 }}).toArray()
const { length } = items || []

for (let i = 0; i < length; i++) {
  const { _id, created_at, tags } = items[i]
  const { length: tagsLength } = tags || []
  for (let j = 0; j < tagsLength; j++) {
    const doc = {
      tag: tags[j],
      image: _id,
      created_at,
      rating: supportedLocales.map(locale => ({ [locale]: 0 }))
    }
    await imagesTagsCollection.insertOne(doc)
      .catch(error => log(`image - ${_id}, tag - ${tags[j]} --> insert error: ${JSON.stringify(error)}`))
  }

  console.log(_id)
}


client.close()
