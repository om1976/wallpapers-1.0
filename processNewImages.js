import dotenv from 'dotenv'
import { client, db } from './libs/mongo.js'
import { transliterate } from './libs/transliterate.js';
import { log } from './libs/log.js';

dotenv.config()

const supportedLocales = ['en', 'ru', 'es', 'pt', 'fr', 'de', 'ua', 'ja', 'cn']

const tagsCollection = db.collection(process.env.MONGO_MOB_TAGS_COLLECTION)
const [lastTag] = await tagsCollection.find().sort({ _id: -1 }).limit(1).toArray()
let lastTagId = lastTag['_id']

const getTagId = async (name, slug, isCategory = false) => {
  const [tag] = await tagsCollection.find({ $and: [{ 'names.slug': slug }, { 'names.locale': 'ru' }]}).toArray()
  if (tag) {
    return tag._id
  }
  const _id = lastTagId + 1
  const newTag = {
    _id,
    isCategory,
    isColor: false,
    names: [{
      locale: 'ru',
      name,
      slug,
    }]
  }
  return await tagsCollection.insertOne(newTag).then(() => {
    lastTagId++
    return _id
  }).catch(() => {})
}

const getTags = async (rawTags) => {
  const { length: tagsLength } = rawTags || []
  const tags = []
  for (let j = 0; j < tagsLength; j++) {
    const slug = transliterate(rawTags[j]['text'])
    const name = `${rawTags[j]['text'].charAt(0).toUpperCase()}${rawTags[j]['text'].slice(1)}`
    const _id = await getTagId(name, slug)
    if (_id) {
      tags.push({ _id, slug })
    }
  }
  return tags
}

const newImagesCollection = db.collection(process.env.MONGO_IMAGES_COLLECTION)
const imagesCollection = db.collection(process.env.MONGO_MOB_IMAGES_COLLECTION)
const imagesTagsCollection = db.collection(process.env.MONGO_MOB_IMAGES_TAGS_COLLECTION)
const imagesListCollection = db.collection(process.env.MONGO_MOB_IMAGES_LIST_COLLECTION)
const [lastImage] = await imagesCollection.find().sort({ _id: -1 }).limit(1).toArray()
let lastImageId = lastImage['_id']
const items = await newImagesCollection.find({ size: { $ne: null }}).toArray()
const { length } = items || []
const imagesList = []
const imagesTagsList = []


for (let i = 0; i < length; i++) {
  const { url, size, width, height, created_at, downloads, tags: rawTags, rating, votes, author, authorLink, license, category } = items[i]
  const _id = lastImageId + 1
  const tags = await getTags(rawTags)
  const categorySlug = category.href.split('/')[2]
  const categoryId = await getTagId(category.text, categorySlug, true)

  if (!tags.find(tag => categoryId === tag._id)) {
    tags.unshift({ _id: categoryId, slug: categorySlug })
  }

  const doc = {
    _id,
    url,
    size,
    width,
    height,
    created_at,
    downloads,
    tags: tags.map(tag => tag._id),
    src: tags.map(tag => tag.slug).join('-'),
    _legacy_order: 0,
    metadata: { rating, votes, author, authorLink, license },
  }

  await imagesCollection.insertOne(doc)
    .then(() => {
      lastImageId++
      const imageListDoc = {
        image: _id,
        created_at,
        rating: supportedLocales.map(locale => ({ [locale]: 0 }))
      }
      imagesList.push(imageListDoc)
      tags.forEach(tag => {
        imagesTagsList.push({
          ...imageListDoc,
          tag: tag._id
        })
      })
    })
    .catch(error => log(`${url} -- insert error: ${JSON.stringify(error)}`))

  console.log(_id)
}

await imagesTagsCollection.insertMany(imagesTagsList).catch(error => log(`imagesTagsList -- insert error: ${JSON.stringify(error)}`))
await imagesListCollection.insertMany(imagesList).catch(error => log(`imagesList -- insert error: ${JSON.stringify(error)}`))

client.close()
