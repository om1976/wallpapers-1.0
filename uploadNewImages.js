import fs  from 'fs'
import FormData from 'form-data'
import axios from 'axios'
import dotenv from 'dotenv'
import { client, db } from './libs/mongo.js'
import { log } from './libs/log.js'

dotenv.config()

const processRemoteFile = async (path) => {
  const form = new FormData();
  form.append('file', fs.createReadStream(path));

  const requestConfig = {
    headers: form.getHeaders(),
    maxContentLength: Infinity,
    maxBodyLength: Infinity,
  };

  return axios.post(
    `${process.env.HOST_IMAGES_UPLOAD}${process.env.PATH_IMAGES_UPLOAD}`,
    form,
    requestConfig,
  ).then(({ data }) => data)
    .catch((response) => ({ error: response }))
}

const imagesCollection = db.collection(process.env.MONGO_IMAGES_COLLECTION)
const items = await imagesCollection.find({}).toArray()
const { length } = items || []

for (let i = 0; i < length; i++) {
  const result = await processRemoteFile(items[i].path)
  if (result.error) {
    log(`${items[i]['src']} -- upload error: ${JSON.stringify(result.error)}`)
    continue
  }
  const { url, metadata } = result
  const { size, width, height } = metadata || {}
  const created_at = new Date(items[i]['date']).getTime() / 1000
  await imagesCollection.updateOne({ _id: items[i]['_id'] }, { $set: { created_at, url, size, width, height }})
    .catch(() => log(`${items[i]['src']} -- update error`))
  console.log(items[i]['src']);
}

client.close()
