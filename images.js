import axios from 'axios'
import cheerio from 'cheerio'
import dotenv from 'dotenv'
import { client, db } from './libs/mongo.js'
import { downloadFile } from './libs/download.js'
import { sleep } from './libs/sleep.js'
import { log } from './libs/log.js'

dotenv.config()

const processPage = async (url) => axios.get(`${process.env.HOST}${url}`)
  .then(response => {
    const $ = cheerio.load(response.data)
    const [category] = $('.filter.filter_selected .filter__link').map((i, el) => ({
      href: decodeURI(el.attribs.href),
      text: el.children[0].data.trim()
    })).get()
    const tags = $('.wallpaper__tags a').map((i, el) => ({
      href: decodeURI(el.attribs.href),
      text: el.children[0].data.trim()
    })).get()
    const id = url.split('_').pop()
    const src = url.split('/').pop()
    const [resolution] = $('.gui-button_full').map((i, el) => el.attribs.href.split('/').pop()).get()
    const author = $('.author__row:contains("Автор:")').text().split(':').pop().trim()
    const license = $('.author__row:contains("Лицензия:")').text().split(':').pop().trim()
    const downloads = $('span:contains("Скачиваний")').next().text().trim()
    const date = $('span:contains("Загружено")').next().text().trim()
    const rating = $('.JS-Vote-Rating').text().trim()
    const votes = $('.JS-Vote-Total').text().trim()
    const authorLinkNode = $('.author__link').get()[0]
    const authorLink = authorLinkNode && authorLinkNode.attribs.href
    const imageUrl = `${process.env.HOST_IMAGES}/image/${src}_${resolution}.jpg`
    const path = `${process.env.DOWNLOAD_PATH}/${src}.jpg`
    return {
      url,
      id,
      category,
      tags,
      resolution,
      downloads,
      date,
      rating,
      votes,
      author,
      authorLink,
      license,
      src,
      imageUrl,
      path,
    }
  })
  .catch((error) => ({ error }))

const allValues = await db.collection(process.env.MONGO_PAGES_COLLECTION)
  .aggregate([
    {
      $unwind: '$links',
    },
    {
      $group:
        {
          _id: null,
          items: { $addToSet: "$links" }
        }
      }
  ])
  .toArray()

const { items } = allValues[0] || {}
const { length } = items || []
const processed = await db.collection(process.env.MONGO_IMAGES_COLLECTION).find({}).toArray()
const unprocessedItems = items.filter(item => processed.findIndex(el => el.url === item) === -1)

for (let i = 0; i < length; i++) {
  await sleep(1000);
  const result = await processPage(unprocessedItems[i])
  const { imageUrl, path } = result
  if (imageUrl) {
    await downloadFile(imageUrl, path)
      .then(() => {
        console.log('processed -->', imageUrl)
        db.collection(process.env.MONGO_IMAGES_COLLECTION).insertOne(result)
      })
      .catch(() => log(`download error --> ${imageUrl}`))
  }
  if (result.error) {
    log(`parse error --> ${unprocessedItems[i]}`)
  }
}

client.close()
