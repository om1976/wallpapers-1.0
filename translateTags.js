import axios from 'axios'
import chunk from 'lodash.chunk'
import dotenv from 'dotenv'
import { db } from './libs/mongo.js'
import { sleep } from './libs/sleep.js'
import { log } from './libs/log.js';

dotenv.config()

const subscriptionKey = process.env.MS_API_SUBSCRIPTION_KEY
const subscriptionRegion = process.env.MS_API_SUBSCRIPTION_REGION
const tagsCollection = db.collection(process.env.MONGO_MOB_TAGS_COLLECTION)

const tagsData = await tagsCollection.find({ $where: 'this.names.length === 1' }).toArray()
const namesArray = tagsData.map(item => ({ 'Text': item.names[0].name }))

const chunks = chunk(namesArray, 100)
const chunksLength = chunks.length

const headers = {
  'Content-Type': 'application/json',
  'Ocp-Apim-Subscription-Key': subscriptionKey,
  'Ocp-Apim-Subscription-Region': subscriptionRegion,
}
for (let i = 0; i < chunksLength; i++) {
  await axios.post(
    'https://api.cognitive.microsofttranslator.com/translate?api-version=3.0&from=ru&to=en&to=de&to=es&to=fr&to=ja&to=pt&to=ru&to=uk&to=zh-Hans',
    chunks[i],
    { headers }
  )
  .then(response => {
    const { data } = response
    const dataLength = data.length
    for (let j = 0; j < dataLength; j++) {
      const { translations } = data[j]
      const translationRu = translations.find(translation => translation.to === 'ru')
      const tagRu = tagsData.find(tag => tag.names[0].name === translationRu.text)
      const slug = tagRu.names[0].slug
      const localesMap = { 'zh-Hans': 'cn', 'uk': 'ua' }
      const names = translations.map(item => ({ name: item.text, locale: localesMap[item.to] || item.to, slug }))
      tagsCollection.updateOne({ _id: tagRu['_id'] }, { $set: { names } })
        .catch((error) => log(`${tagRu['_id']} -- ${JSON.stringify(error)}`))
      console.log({ _id: tagRu['_id'], names })
    }
  })
  .catch((error) => {
    console.log(error)
  })
  await sleep(3000)
}
