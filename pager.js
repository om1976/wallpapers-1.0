import axios from 'axios'
import cheerio from 'cheerio'
import dotenv from 'dotenv'
import { db } from './libs/mongo.js'
import { sleep } from './libs/sleep.js'

dotenv.config()

const host = process.env.HOST
const skip = 0
const limit = 7336

for (let i = 1; i <= limit; i++) {
  await sleep(1000);
  await axios
    .get(`${host}/all/page${skip + i}`)
    .then(response => {
      const $ = cheerio.load(response.data)
      const links = $('.wallpapers__list .wallpapers__link').map((i, el) => el.attribs.href).get()
      db.collection(process.env.MONGO_PAGES_COLLECTION).insertOne({
        href: `/all/page${skip + i}`,
        links,
      })
      console.log(`/all/page${skip + i} ---> OK`)
    })
    .catch(() => {
      db.collection(process.env.MONGO_PAGES_COLLECTION).insertOne({ href: `/all/page${skip + i}` })
      console.log(`/all/page${skip + i} ---> ERROR`)
    })
}
